#!/usr/bin/python3
# -*- coding: utf-8 -*-

import argparse
import datetime
import json
import os
from harvest.harvest import HarvestPATokenClient, TimeEntries, MyProjects
from harvest.config import Config
from harvest.utils import Utils, termcolors


def main():
	# Arguments parser
	parser = argparse.ArgumentParser(description="Harvest Timesheets Command Line Utility.")
	parser.add_argument("-p", "--projects", action="store_true", dest="projects", help="List all projects the configured harvest account has access to.")
	parser.add_argument("--start", type=str, help="A string representing the date, in ISO 8601 format (‘YYYY-MM-DD’), where to start filling the timesheet.")
	parser.add_argument("--end", type=str, help="A string representing the date, in ISO 8601 format (‘YYYY-MM-DD’), which will be the last day filled in the timesheet.")
	args = parser.parse_args()
	
	config_file = ".harvest/config.json"
	home = os.path.expanduser("~")

	"""
		GET CONFIGURATION

		TODO - Generate config file if it do not exists
	"""
	file = os.path.abspath(os.path.join(home, config_file))
	if not os.path.exists(file):
		# config = Config()
		# config.generate_file(file)
		return

	config = Config(file)

	# Build API Auth Client
	client = HarvestPATokenClient("https://api.harvestapp.com", "v2", config.harvest.token, config.harvest.account_id)

	"""
		LIST HARVEST PROJECTS
	"""
	if args.projects:
		print("%sListing projects from harvest...%s" % (termcolors.HEADER, termcolors.ENDC))
		endpoint = MyProjects(client)
		response = endpoint.get().json()
		project_assignments = response.get("project_assignments")
		for project_assignment in project_assignments:
			project_info = project_assignment.get("project")
			client = project_assignment.get("client")
			task_assignments = project_assignment.get("task_assignments")

			print(u"%sPROJECT %i%s: %s - %s" % (termcolors.OKBLUE, project_info.get("id"), termcolors.ENDC, project_info.get("code"), project_info.get("name")))
			for task_assignment in task_assignments:
				task = task_assignment.get("task")
				print(u"\t|___%sTASK %i%s: %s" % (termcolors.OKGREEN, task.get("id"), termcolors.ENDC, task.get("name")))
		
		return


	"""
		CONVERT START/END ARGS INTO DATES

	"""
	if not args.start:
		start = datetime.date.today()
	else:
		start = Utils.fromisoformat(args.start)

	if not args.end:
		end = datetime.date.today()
	else:
		end = Utils.fromisoformat(args.end)


	"""
		UPDATE HARVEST TIMESHEETS

		TODO - Check for timesheet entries before posting new values
		TODO - Add a parameter for max worked hours per day, and complete if not reach for this day
	"""
	endpoint = TimeEntries(client)
	task = config.harvest.task

	# For each day between the given range...
	delta = end - start
	for d in range(0, delta.days + 1):

		# Compute this day date
		day = start + datetime.timedelta(days=d)

		# Skip if this day is in exclude list
		if day.isoweekday() in config.exclude:
			continue

		# Post to harvest
		response = endpoint.post(params={"project_id": task.project_id, "task_id": task.task_id, "notes": task.notes, "hours": task.hours, "spent_date": day.isoformat()})
		
		# Print response
		if response.status_code == 201:
			print(u"%s%s%s successfully set." % (termcolors.OKBLUE, day.isoformat(), termcolors.ENDC))
		else:
			print(u"Harvest responded with status code: %s%d%s." % (termcolors.FAIL, response.status_code, termcolors.ENDC))
		


if __name__ == "__main__":
	main()