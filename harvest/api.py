#!/usr/bin/python3

import requests

"""
	Base Class for RESTful API endpoints.
	Do not instansiate it. It's meant to be inherited.

"""
class EndPoint:

	"""
		Constructor

	"""
	def __init__(self, client):
		self._client = client


	"""
		Return the full URI of the API endpoint.

	"""
	def get_url(self):
		return u"%s/%s" % (self._client.get_base_url(), self._path)


	"""
		Make an HTTP GET request to the endpoint with the given parameters.
		Available parameters are handled by the children.
		Returns a Response object

	"""
	def get(self, **params):	
		return  requests.get(url=self.get_url(), headers=self._client.get_auth_header(), params=params)


	"""
		Make an HTTP POST request to the endpoint with the given arguments.
		Arguments can be either "params", for query params and/or "data" for body data.
		Available parameters and data are handled by the children.
		Returns a Response object

	"""
	def post(self, **args):
		params = args.get('params', {})
		data = args.get('data', {})

		return requests.post(url=self.get_url(), headers=self._client.get_auth_header(), params=params, data=data)
