#!/usr/bin/python3
import re
import datetime


"""
	Helper class to output colors in terminal
"""
class termcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


"""
	Container for utilitaries methods
"""
class Utils:

	@staticmethod
	def fromisoformat(date_string):
		"""
			Return a date object from an ISO formatted date string

			param > string date_string
			return < datetime.date
		"""

		if not re.match(r"^[0-9]{4}-[0-9]{2}-[0-9]{2}$", date_string):
			raise TypeError

		date = date_string.split("-")
		return datetime.date(int(date[0]), int(date[1]), int(date[2]))

