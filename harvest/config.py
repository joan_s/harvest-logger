#!/usr/bin/python3

import os
import json


"""
	TODO - Rework everything -_-"
	TODO - More solid json serialization/deserialization implementation
	TODO - Implements config file generation method
"""
class Config:

	def __init__(self, config = None):
		self.harvest = Harvest()
		self.google = Google()
		self.mapping = {}
		self.exclude = {}

		if isinstance(config, str):
			self.load_from_file(config)
		elif isinstance(config, dict):
			self.load(config)


	def load_from_file(self, config_file):
		if not os.path.exists(config_file):
			return

		stream = open(config_file, "r")
		config = json.load(stream)
		self.load(config)
	

	def load(self, config):
		self.mapping = config.get("mapping", {})
		self.exclude = config.get("exclude", {})
		self.harvest.load(config.get("harvest", {}))
		self.google.load(config.get("google", {}))


	def generate_file(self, config_file):
		raise NotImplementedError


class Harvest:

	def __init__(self):
		self.token = "YOUR_PERSONAL_ACCESS_TOKEN"
		self.account_id = "YOUR_ACCOUNT_ID"
		self.task = Task()

	def load(self, config):
		self.token = config.get("token", None)
		self.account_id = config.get("account_id", None)
		self.task.load(config.get("task", Task()))



class Task:

	def __init__(self):
		self.project_id = None
		self.task_id = None
		self.notes = None
		self.hours = None

	def load(self, config):
		self.project_id = config.get("project_id")
		self.task_id = config.get("task_id")
		self.notes = config.get("notes")
		self.hours = config.get("hours")



class Google:
	
	def __init__(self):
		self.calendars = []


	def load(self, config):
		self.calendars = config.get("calendars", [])