#!/usr/bin/python3

import base64

"""
	Base Class for HTTP Requests Authentication methods.
	Do not instantiate it. It's meant to be inherited.

"""
class ClientManager:
   
	def __init__(self, uri, version):
		self._uri = uri
		self._version = version
		self._authorization = "Authorization"

   
	def get_base_url(self):
		return u"%s/%s" % (self._uri, self._version)


	def get_auth_header(self):
		return {
			self._authorization: u"%s %s" % (self._type, self.get_credentials())
		}


"""
	Personnal Access Token Authentication method.

"""
class PATokenClient(ClientManager):

	_type = "Bearer"
	
	def __init__(self, uri, version, token):
		super().__init__(uri, version)
		self._token = token


	def get_credentials(self):
		return self._token


"""
	Basic Authentication method.

"""
class BasicClient(ClientManager):

	_type = "Basic"

	def __init__(self, uri, version, user, password):
		super().__init__(uri, version)
		self._user = user
		self._password = password


	def get_credentials(self):
		pass_string = u"%s:%s" % (self._user, self._password)
		return base64.b64encode(pass_string)