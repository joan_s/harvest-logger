#!usr/bin/python3

from harvest.api import EndPoint
from harvest.auth import PATokenClient 

class HarvestPATokenClient(PATokenClient):

	_harvest_account_id = "Harvest-Account-ID"

	def __init__(self, uri, version, token, account_id):
		super().__init__(uri, version, token)
		self._account_id = account_id

	# @Override
	def get_auth_header(self):
		return {
			self._authorization: u"%s %s" % (self._type, self.get_credentials()),
			self._harvest_account_id: self._account_id
		}


class TimeEntries(EndPoint):

	_path = "time_entries"


class MyProjects(EndPoint):

	_path = "users/me/project_assignments"
