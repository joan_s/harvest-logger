from setuptools import setup, find_packages

setup(
	name='harvest',
	author='Joan SOULIARD',
	author_email='joan.souliard@gmail.com',
	version='0.0.1',
	description='Implementation of the Harvest API in Python',
	long_description="".join(open("README.md")),
	url='https://gitlab.com/JSouliard/harvest-logger',
	license='MIT',
	packages=[
		"harvest"
	],
	python_requires='>=3.6',
	install_requires=[
		'requests==2.19.1',
		# 'google-api-python-client==1.7.8',
		# 'google-auth-httplib2==0.0.3',
		# 'google-auth-oauthlib==0.3.0',
		# 'python-dateutil==2.8.0'
	],
	entry_points={
		'console_scripts': [
			'hlog=harvest.cli:main'
		]
	}
)
