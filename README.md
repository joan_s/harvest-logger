# Harvest Logger

## What is it ?

A command line utility to log it's time into Harvest Timesheet.

## Setup

```sh
pip install git+https://gitlab.com/JSouliard/harvest-logger@latest
```

## Configuration

Create a `~/.harvest/config.json` file. 

The configuration take the following structure:

```json
{
	"harvest": 
	{
		"token": "YOUR_HARVEST_PERSONNAL_ACCESS_TOKEN",
		"account_id": "YOUR_HARVEST_ACCOUNT_ID",
		"task": 
		{
			"project_id": "TASK_PROJECT_ID",
			"task_id": "TASK_ID",
			"notes": "YOUR_NOTE",
			"hours": "TASK_DURATION"
		}
	},
	"exclude": [ 6, 7 ]
}
```

`task` is an object descibing the task to use to fill the timesheet. 
Once `token` and `account_id` are specified you can use `-p` or `--projects` argument to list projects and tasks associated with your Harvest account. 
You can then specify the `project_id` and `task_id` of the task you wish to be used to fill your timesheet. You need to input the amount of time you want to be filled for this task in the `hours` property. 
`notes` is optional.

`exclude` is an array containing days of the week to ignore when filling the timesheet, as integer, where Monday is 1 and Sunday is 7.

## Usage

```sh
hlog --start 2019-03-21 --end 2019-04-01
```

if `--start` or `--end` arguments are not specified they default to the system current date.

You can add it to a cronjob, for it to log your time everyday automatically.

```cron
30 17 * * * /usr/local/bin/hlog
```

## License

MIT License

Copyright (c) 2019 Joan Souliard

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
